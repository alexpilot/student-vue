class Student < ApplicationRecord
  enum gender: [:male, :female]
  validates :first_name, length: { in: 2..40 }, presence: true
  validates :middle_name, length: { in: 2..60 }, presence: false
  validates :last_name, length: { in: 2..40 }, presence: true
  validates :nick_name, uniqueness: true, presence: true
  validates :gender, presence: true
end
