class StudentsController < ApplicationController

  def index
      @students = Student.all
      render json: @students
  end
  

  def create
    @student= Student.new(student_params)
    if @student.save
      render json: { status: :ok, message: 'Success save' }
    else
      render json: { message: "Validation failed", errors: @student.errors}, status: 400 
    end
  end

  def show
    @student = Student.find(params[:id])
    render json: { data: @student, status: :ok, message: 'Success show' }
  end

  def update
    @student = Student.find(params[:id])
    if @student.update(student_params)
      render json: { status: :ok, message: 'Success update' }
    else
      render json: { message: "Validation failed", errors: @student.errors}, status: 400 
    end
  end

  def destroy
    @student = Student.find(params[:id])
    if @student.destroy
      render json: { json: 'Student record was successfully deleted.'}
    else
      render json: { errors: @student.errors, status: :unprocessable_entity }
    end
  end

  private

  def student_params
    params.require(:student).permit(:id, :first_name, :middle_name, :last_name, :nick_name, :gender)
  end
end
