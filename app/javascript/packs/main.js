/* eslint no-console: 0 */
// Run this example by adding <%= javascript_pack_tag 'hello_vue' %> (and
// <%= stylesheet_pack_tag 'hello_vue' %> if you have styles in your component)
// to the head of your layout file,
// like app/views/layouts/application.html.erb.
// All it does is render <div>Hello Vue</div> at the bottom of the page.

import Vue from 'vue'
var Turbolinks = require("turbolinks")
Turbolinks.start()
import TurbolinksAdapter from 'vue-turbolinks'
import VueResource from 'vue-resource'
import '@mdi/font/css/materialdesignicons.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import '@fortawesome/fontawesome-free/css/all.css' // Ensure you are using css-loader
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import App from '../app.vue'
import axios from 'axios'
Vuetify : new Vuetify()
Vue.use(Vuetify)

Vue.use(VueResource)
Vue.use(TurbolinksAdapter)
export default new Vuetify({
  icons: {
    iconfont: 'mdi', // default - only for display purposes
    iconfont: 'md',
    iconfont: 'fa',
  },
})
document.addEventListener('turbolinks:load', () => {
  axios.defaults.headers.common['X-CSRF-Token'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content')

  const app = new Vue({
    vuetify: new Vuetify(),
    render: h => h(App),
  }).$mount()
  document.body.appendChild(app.$el)

  console.log(app)
})

