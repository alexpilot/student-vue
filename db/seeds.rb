# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#


10.times do |x|
  num = x.even? ? 0:1
  Student.create( { first_name: "Name #{x}", middle_name: "MiddleName #{x}", last_name: "LastName #{x}", gender: num, nick_name: "nickname #{x}" } )
end
