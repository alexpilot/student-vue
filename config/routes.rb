Rails.application.routes.draw do
  root to: "main#index"
  get 'main/index'

  resources :students
  get "app", to: "students#index"
end
